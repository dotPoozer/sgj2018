﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//using LeanTween;

public class LevelManager : MonoBehaviour {
    private Pipe[,] pipes;
    private bool[,] connectivity;

    public int levelWidth = 13;
    public int levelHeight = 13;
    public int sourceHeight = 6;

    public int WinningScore = 4;

    public int MaxConnectionsOnLevelStart = 50;
    [HideInInspector]
    public int lastIndX;
    [HideInInspector]
    public int lastIndY;

    public Sprite sourceTexture;

    public float spacing = 20.0f;

    public GameObject[] pipeTiles = new GameObject[4];

    public int[] typeFrequency = { 3, 3, 2, 1 };
    public int[,] directionVectors = new int [4,2] { {0, -1}, {1, 0}, {0, 1}, {-1, 0} };

    public GameObject prefabEndPointRight;
    public GameObject prefabEndPointLeft;
    GameObject endPointRight;
    GameObject endPointLeft;
    int heightEndPointRight;
    int heightEndPointLeft;

    public Transform Parent;

    public void RotatePipeAt(bool left, int x, int y)
    {
        if(x >= 0 && x < levelWidth && y >= 0 && y < levelHeight)
        {
            pipes[x, y].SetOrientation(pipes[x, y].orientation + (left?-1:1));
            pipes[x, y].AnimateOrientation();
            UpdateConnections();
        }
    }

    public Vector3 PipePositionAt(int x, int y)
    {
        if (x >= 0 && x < levelWidth && y >= 0 && y < levelHeight)
        {
            return pipes[x, y].transform.position;
        }

        return Vector3.zero;
    }

    public void MoveEndPoint(bool left, bool up) {
        if(left) {
            if(up) {
                if(heightEndPointLeft == 0) return;
                --heightEndPointLeft;
            } else {
                if(heightEndPointLeft == lastIndY) return;
                ++heightEndPointLeft;
            }
            LeanTween.moveY(endPointLeft, endPointLeft.transform.position.y + spacing * (up ? 1 : -1), 0.2f).setEase(LeanTweenType.easeInOutCubic);
            //endPointLeft.transform.position += new Vector3(0, spacing * (up ? 1 : -1));
            MoveSfx(endPointLeft);
        } else {
            if(up) {
                if(heightEndPointRight == 0) return;
                --heightEndPointRight;
            } else {
                if(heightEndPointRight == lastIndY) return;
                ++heightEndPointRight;
            }
            LeanTween.moveY(endPointRight, endPointRight.transform.position.y + spacing * (up ? 1 : -1), 0.2f).setEase(LeanTweenType.easeInOutCubic);
            //endPointRight.transform.position += new Vector3(0, spacing * (up ? 1 : -1));
            MoveSfx(endPointRight);
        }
        CheckScoreCondition();
    }

    void MoveSfx(GameObject ship) {
        var moveSfxEmitter = ship.GetComponents<FMODUnity.StudioEventEmitter>()[0];
        moveSfxEmitter.Play();
    }

    IEnumerator RoundSwitch() {
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = false;
        yield return new WaitForSeconds(2.0f);
        ClearLevel();
        Generate();
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = true;
    }

    IEnumerator Win1() {
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = false;
        yield return new WaitForSeconds(2.0f);
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = true;
        var target = GameObject.Find("MusicEmitter");
        var emitter = target.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.Stop();

        var target2 = GameObject.Find("WinEmitter");
        var emitter2 = target2.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter2.Play();

        ServiceLocator.Instance.SceneService.ChangeScene("Win1");
    }

    IEnumerator Win2() {
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = false;
        yield return new WaitForSeconds(2.0f);
        ServiceLocator.Instance.PlayerControllerService.inputEnabled = true;
        var target = GameObject.Find("MusicEmitter");
        var emitter = target.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.Stop();
        var target2 = GameObject.Find("WinEmitter");
        var emitter2 = target2.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter2.Play();
        ServiceLocator.Instance.SceneService.ChangeScene("Win2");
    }

    void CheckScoreCondition() {
        bool wonA = connectivity[0, heightEndPointLeft] && pipes[0, heightEndPointLeft].Connects(3);
        bool wonB = connectivity[lastIndX, heightEndPointRight] && pipes[lastIndX, heightEndPointRight].Connects(1);
        if (wonA || wonB)
        {
            if (wonA)
            {
                ++ServiceLocator.Instance.ScoringService.ScoreA;
            }
            else
            {
                ++ServiceLocator.Instance.ScoringService.ScoreB;
            }

            if(ServiceLocator.Instance.ScoringService.ScoreA >= WinningScore) {
                StartCoroutine(Win1());
            }
            else if (ServiceLocator.Instance.ScoringService.ScoreB >= WinningScore)
            {
                StartCoroutine(Win2());
            }
            else
            {
                StartCoroutine(RoundSwitch());
            }
        }
        
    }

    void ClearLevel() {
        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                Destroy(pipes[x, y].gameObject);
            }
        }
    }

    void Generate() {
        int totalFreq = typeFrequency[0] + typeFrequency[1] + typeFrequency[2] + typeFrequency[3];
        pipes = new Pipe[levelWidth, levelHeight];
        connectivity = new bool[levelWidth, levelHeight];
        int halfX = levelWidth / 2;
        int halfY = levelHeight / 2;
        Vector2 startpoint = new Vector2(-(halfX) * spacing, (halfY) * spacing);
        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                int type = 0;
                bool source = false;
                if(x == halfX && y == sourceHeight) {
                    type = 3;
                    source = true;
                } else {
                    int pick = Random.Range(0, totalFreq);
                    int total = 0;
                    for(int i = 0; i < 4; ++i) {
                        total += typeFrequency[i];
                        if(pick < total) {
                            type = i;
                            break;
                        }
                    }
                }
                Pipe instance = Instantiate(pipeTiles[type], Parent).GetComponent<Pipe>();
                pipes[x, y] = instance;
                instance.transform.position = startpoint + new Vector2(x * spacing, -y * spacing);
                int orientation = Random.Range(0, 4);
                instance.SetOrientation(orientation);
                if(source) {
                    instance.GetComponentInChildren<SpriteRenderer>().sprite = sourceTexture;
                }
            }
        }
        MakePipesFair();
        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                pipes[x, y].AnimateOrientation();
            }
        }
        UpdateConnections();
    }
    
    void MakePipesFair() {
        // Naive greedy algorithm.
        // Figure out on which side do we have more pipe pairs connected, then rotate random pipes
        // on that side until the connection count reduces.

        // This will count every connection twice (lazy but still correct).
        int[] biConnectionCount = { 0, 0 };
        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                if (x * 2 == levelWidth - 1) {
                    continue;
                }
                int side = x * 2 > levelWidth - 1 ? 1 : 0;
                for (int dir = 0; dir < directionVectors.GetLength(0); dir++) {
                    biConnectionCount[side] += PipesBiConnected(x, y, dir) ? 1 : 0;
                }
            }
        }
        Debug.Log("Initial Fairness: " + biConnectionCount[0] + " vs " + biConnectionCount[1]);

        int maxRotationAttempts = levelHeight * levelWidth / 2;
        for (int i = 0; i < maxRotationAttempts; i++) {
            if (biConnectionCount[0] == biConnectionCount[1] && biConnectionCount[0] <= MaxConnectionsOnLevelStart) {
                break;
            }
            int y = Random.Range(0, levelHeight);
            int x = Random.Range(0, (levelWidth - 1)/2);
            int rotatedSide = 0;
            if (biConnectionCount[0] < biConnectionCount[1]) {
              rotatedSide = 1;
              x += (levelWidth - 1)/2 + 1;
            }
            int initialConnections = HowManyBiConnections(x, y);
            int[] biConnectionsForOrientation = new int[4];
            for (int o = 0; o < 4; o++) {
              pipes[x,y].SetOrientation(o);
              biConnectionsForOrientation[o] = HowManyBiConnections(x, y);
            }
            pipes[x,y].SetOrientation(System.Array.IndexOf(biConnectionsForOrientation, biConnectionsForOrientation.Min()));
            biConnectionCount[rotatedSide] -= (initialConnections - biConnectionsForOrientation.Min()) * 2;
            Debug.Log("Fixed Fairness: " + biConnectionCount[0] + " vs " + biConnectionCount[1]);
        }
    }

    // Returns true if there is a bidirectional connection from pipe at [x, y] in direction dir.
    // Handles level boundaries safely.
    bool PipesBiConnected(int ax, int ay, int dir) {
        int bx = ax + directionVectors[dir,0];
        int by = ay + directionVectors[dir,1];
        if (!CoordinatesWithinLevel(bx, by)) {
          return false;
        }
        int oppositeDir = (dir + 2) % 4;
        return pipes[ax,ay].Connects(dir) && pipes[bx,by].Connects(oppositeDir);
    }

    int HowManyBiConnections(int x, int y) {
        int conn = 0;
        for (int dir = 0; dir < directionVectors.GetLength(0); dir++) {
            conn += PipesBiConnected(x, y, dir) ? 1 : 0;
        }
        return conn;
    }

    bool CoordinatesWithinLevel(int x, int y) {
       return x < levelWidth && x >= 0 && y < levelHeight && y >= 0;
    }

    void Start () {
        lastIndX = levelWidth - 1;
        lastIndY = levelHeight - 1;
        sourceHeight = Mathf.Clamp(sourceHeight, 0, lastIndY);

        Generate();

        int halfX = levelWidth / 2;
        int halfY = levelHeight / 2;
        Vector2 startpoint = new Vector2(-(halfX) * spacing, (halfY) * spacing);

        heightEndPointLeft = levelHeight / 2;
        heightEndPointRight = heightEndPointLeft;
        endPointLeft = Instantiate(prefabEndPointLeft, Parent);
        endPointRight = Instantiate(prefabEndPointRight, Parent);
        endPointLeft.transform.position = new Vector3(startpoint.x-spacing, 0, 0);
        endPointRight.transform.position = new Vector3(-startpoint.x+spacing, 0, 0);

        ServiceLocator.Instance.PlayerControllerService.Initialize(this);
    }

    void Update () {
        bool left = Input.GetMouseButtonDown(0);
        bool right = Input.GetMouseButtonDown(1);
        if(left || right) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.y = -pos.y;
            pos += new Vector3((levelWidth / 2) * spacing + spacing * 0.5f, (levelHeight / 2) * spacing + spacing * 0.5f, 0);

            int x = (int)pos.x;
            int y = (int)pos.y;

            RotatePipeAt(left, x, y);
        }
    }

    void UpdateConnections() {
        int halfX = levelWidth / 2;

        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                connectivity[x, y] = false;
            }
        }

        connectivity[halfX, sourceHeight] = true;

        bool anyFound = true;
        while(anyFound) {
            anyFound = false;
            for(int y = 0; y < levelHeight; ++y) {
                for(int x = 0; x < levelWidth; ++x) {
                    if(connectivity[x, y]) {
                        for(int dir = 0; dir < directionVectors.GetLength(0); dir++) {
                            int bx = x + directionVectors[dir,0];
                            int by = y + directionVectors[dir,1];
                            if (!CoordinatesWithinLevel(bx, by)) {
                                continue;
                            }
                            if(!connectivity[bx, by] && PipesBiConnected(x, y, dir)) {
                                connectivity[bx, by] = true;
                                anyFound = true;
                            }
                        }
                    }
                }
            }
        }
        for(int y = 0; y < levelHeight; ++y) {
            for(int x = 0; x < levelWidth; ++x) {
                pipes[x, y].SetMarked(connectivity[x, y]);
            }
        }
        CheckScoreCondition();
    }
}
