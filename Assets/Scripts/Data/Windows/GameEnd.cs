﻿namespace Data.Windows
{
    public class GameEnd : Window
    {
        public void MainMenu()
        {
            ServiceLocator.Instance.ScoringService.Reset();
            ServiceLocator.Instance.SceneService.ChangeScene("MainMenu");
            ServiceLocator.Instance.WindowsService.Close();
        }

        public void Restart()
        {
            ServiceLocator.Instance.ScoringService.Reset();
            ServiceLocator.Instance.SceneService.ChangeScene("Game");
            ServiceLocator.Instance.WindowsService.Close();
        }
    }
}