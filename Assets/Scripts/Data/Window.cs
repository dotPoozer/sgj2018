﻿using UnityEngine;

namespace Data
{
    public class Window : MonoBehaviour
    {
        public string ParentScene;

        public virtual void Close() { }
    }
}