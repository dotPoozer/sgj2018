﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : MonoBehaviour {
    public void ReturnToMenu() {
        Debug.Log("Quitting");
        ServiceLocator.Instance.ScoringService.Reset();
        var target = GameObject.Find("MusicEmitter");
        var emitter = target.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.Play();
        ServiceLocator.Instance.SceneService.ChangeScene("MainMenu");
        ServiceLocator.Instance.WindowsService.Close();
    }
}
