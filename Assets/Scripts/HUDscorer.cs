﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDscorer : MonoBehaviour {

    public Image scientist;
    public Image[] fuel = new Image[4];

    private int score = 0;

    public void ScoreUp() {
        fuel[score].color = Color.white;
        ++score;
        LeanTween.moveY(scientist.gameObject, scientist.transform.position.y + 22, 0.14f).setLoopPingPong(5);
    }
}
