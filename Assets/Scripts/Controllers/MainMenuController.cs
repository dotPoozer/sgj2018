﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    private float delay = 0.0f;
    public GameObject[] Buttons;
    private int currentButton;
    public EventSystem EventSystem;

    public void StartGame()
    {
        var target = GameObject.Find("MusicEmitter");
        var emitter = target.GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.SetParameter("MusicState", 1.0f);
        ServiceLocator.Instance.SceneService.ChangeScene("game");
    }

    public void Options()
    {
        ServiceLocator.Instance.WindowsService.Open("OptionsPopup");
    }

    public void Exit()
    {
        #if UNITY_EDITOR
        Debug.Break();
        #else
        Application.Quit();
        #endif
    }


    private void Start()
    {
        EventSystem.SetSelectedGameObject(Buttons[currentButton]);
    }

    private void Update()
    {
        delay += Time.deltaTime;
        var axis = GamePad.GetAxis(CAxis.LY);
        if (Math.Abs(axis) > 0.1 && delay > 0.5f)
        {
            currentButton += (int) Mathf.Sign(axis);
            if (currentButton < 0)
            {
                currentButton = Buttons.Length + currentButton;
            } else if (currentButton >= Buttons.Length)
            {
                currentButton = currentButton % Buttons.Length;
            }
            EventSystem.SetSelectedGameObject(Buttons[currentButton]);
            delay = 0;
        }

        if (GamePad.GetButton(CButton.A))
        {
            Buttons[currentButton].GetComponent<Button>().onClick.Invoke();
        }
    }
}
