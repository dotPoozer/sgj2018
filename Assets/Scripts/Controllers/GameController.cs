﻿using System;
using TMPro;
using UnityEngine;

namespace Controllers
{
    public class GameController : MonoBehaviour {
        public GameObject tauntMonkey1;
        public GameObject tauntMonkey2;

        public HUDscorer pl1score;
        public HUDscorer pl2score;

        public TextMeshProUGUI ScorePlayerA;
        public TextMeshProUGUI ScorePlayerB;

        private void Start()
        {
            ServiceLocator.Instance.ScoringService.ScoreAChanged += score => RedrawScore(ScorePlayerA, score);
            ServiceLocator.Instance.ScoringService.ScoreBChanged += score => RedrawScore(ScorePlayerB, score);

            ServiceLocator.Instance.ScoringService.ResendEvents();

            ServiceLocator.Instance.ScoringService.ScoreAChanged += score => MonkeyTaunt(true);
            ServiceLocator.Instance.ScoringService.ScoreBChanged += score => MonkeyTaunt(false);

            ServiceLocator.Instance.ScoringService.ScoreAChanged += score => pl1score.ScoreUp();
            ServiceLocator.Instance.ScoringService.ScoreBChanged += score => pl2score.ScoreUp();
        }

        private void RedrawScore(TextMeshProUGUI scorePlayer, Int64 newScore)
        {
            scorePlayer.text = newScore.ToString();
        }

        public void MonkeyTaunt(bool first) {
            GameObject monkey = first ? tauntMonkey1 : tauntMonkey2;
            Debug.Log(tauntMonkey1);
            Debug.Log(tauntMonkey2);
            Debug.Log(monkey);
            monkey.SetActive(true);
        }
    }
}