﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

    public int orientation = 0;
    public int type = 0;

    public GameObject sprite;

    private bool[,] connectionsArray =
    {
        {true,false,true,false }, // STRAIGHT
        {true,true,false,false }, // BEND
        {true,true,true,false }, // TRI
        {true,true,true,true } // QUAD
    };

    public void SetOrientation(int o) {
        orientation = o;
        if(orientation < 0) {
            orientation += 4;
        }else if(orientation > 3) {
            orientation -= 4;
        }
    }
    public void AnimateOrientation() {
        LeanTween.rotateZ(this.gameObject, 90 * orientation, 0.2f);
        //sprite.transform.localEulerAngles = new Vector3(0, 0, 90 * o);
    }

    public bool Connects(int dir) {
        return connectionsArray[type,(orientation + dir) % 4];
    }

    public void SetMarked(bool marked) {
        GetComponentInChildren<SpriteRenderer>().color = marked ? Color.white : Color.gray;
    }
}
