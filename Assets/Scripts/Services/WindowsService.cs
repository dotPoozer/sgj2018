﻿using System.Collections.Generic;
using Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services
{
    public class WindowsService : MonoBehaviour
    {
        private Stack<Window> stack = new Stack<Window>();

        public Transform Canvas;

        public void Init()
        {
            SceneManager.sceneUnloaded += PopStack;
            if (Canvas == null)
            {
                var canvas = FindObjectOfType<Canvas>();
                if (canvas != null)
                {
                    Canvas = canvas.transform;
                }
            }
        }

        public void Open(string prefabName)
        {
            var window = Resources.Load<Window>(prefabName);
            var obj = Instantiate(window, Canvas.transform);
            obj.ParentScene = ServiceLocator.Instance.SceneService.CurrentScene;
            stack.Push(obj);
        }

        public void Close()
        {
            var window = stack.Pop();
            window.Close();
            Destroy(window.gameObject);
        }

        private void PopStack(Scene scene)
        {
            if (stack.Count == 0) return;
            var newStack = new Stack<Window>();

            var tmp = stack.Pop();
            var q = new Queue<Window>();
            while (tmp != null)
            {
                if (tmp.ParentScene != scene.name)
                {
                    q.Enqueue(tmp);
                }
                tmp = stack.Pop();
            }

            tmp = q.Dequeue();
            while (tmp != null)
            {
                newStack.Push(tmp);
                tmp = q.Dequeue();
            }

            stack = newStack;
        }
    }
}