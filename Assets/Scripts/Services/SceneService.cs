﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services
{
    public class SceneService : MonoBehaviour
    {
        public string InitialScene;

        private string currentScene;

        public string CurrentScene
        {
            get { return currentScene; }
        }

        public void Init()
        {
            ChangeScene(InitialScene);
        }

        public void ChangeScene(string next)
        {
            if (currentScene != null)
            {
                SceneManager.UnloadSceneAsync(currentScene).completed += operation => LoadSceneAdditive(next);
            }
            else
            {
                LoadSceneAdditive(next);
            }
        }

        private void LoadSceneAdditive(string next)
        {
            currentScene = next;
            SceneManager.LoadSceneAsync(next, LoadSceneMode.Additive);
        }
    }
}