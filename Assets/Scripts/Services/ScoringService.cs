﻿using System;
using UnityEngine;

namespace Services
{
    public delegate void ScoreChanged(Int64 newScore);

    public class ScoringService : MonoBehaviour
    {
        private Int64 scoreA;
        private Int64 scoreB;

        public Int64 ScoreA
        {
            get { return scoreA; }
            set
            {
                scoreA = value;
                if (ScoreAChanged != null)
                {
                    ScoreAChanged(scoreA);
                }
            }
        }

        public Int64 ScoreB
        {
            get { return scoreB; }
            set
            {
                scoreB = value;
                if (ScoreBChanged != null)
                {
                    ScoreBChanged(scoreB);
                }
            }
        }

        public event ScoreChanged ScoreAChanged;
        public event ScoreChanged ScoreBChanged;

        public void ResendEvents()
        {
            if (ScoreAChanged != null) ScoreAChanged(ScoreA);
            if (ScoreBChanged != null) ScoreBChanged(ScoreB);
        }

        public void Reset()
        {
            scoreA = 0;
            scoreB = 0;
            ServiceLocator.Instance.ScoringService.ScoreAChanged = null;
            ServiceLocator.Instance.ScoringService.ScoreBChanged = null;
        }
    }
}