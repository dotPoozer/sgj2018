﻿using DTO;
using UnityEngine;

namespace Services
{
    public class AudioService : MonoBehaviour
    {
        private AudioSource audioSource;

        public AudioRef[] Sounds;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void SetSound(string name)
        {
            foreach (var sound in Sounds)
            {
                if (sound.Name == name)
                {
                    audioSource.clip = sound.Sound;
                    audioSource.Play();
                }
            }
        }

        public void StopSound()
        {
            audioSource.Stop();
        }
    }
}