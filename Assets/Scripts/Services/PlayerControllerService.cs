﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarbonInput;
using UnityEngine;

namespace Services
{
    public class PlayerControllerService : MonoBehaviour
    {
        public GameObject RedPrefab;
        public GameObject BluePrefab;
        public GameObject Monkey1Prefab;
        public GameObject Monkey2Prefab;

        public Vector2Int RedStart;
        public Vector2Int BlueStart;

        public bool inputEnabled = true;

        [Range(0, 5)]
        public float ButtonMoveDelayTime = 0.1f;
        // Delay between moves if in acceleration mode
        [Range(0, 5)]
        public float ButtonMoveAccelTime = 0.1f;
        // Max delay between prev and current keypress that triggers acceleration.
        [Range(0, 5)]
        public float ButtonMoveAccelThresholdTime = 0.1f;
        [Range(0, 5)]
        public float ButtonRotateDelayTime = 0.5f;
        [Range(0, 3)]
        public float ButtonShipDelayTime = 0.2f;
        [Range(0, 1)]
        public float ButtonForceNeeded = 0.7f;

        private Transform red;
        private Transform blue;
        private Transform monkey1;
        private Transform monkey2;

        private Dictionary<Transform, float> buttonMoveDelay = new Dictionary<Transform, float>();
        private Dictionary<Transform, float> buttonRotateDelay = new Dictionary<Transform, float>();
        private Dictionary<Transform, int> buttonAccelState = new Dictionary<Transform, int>();
        private float[] buttonShipDelay = new float[2];
        private LevelManager levelManager;

        public void Initialize(LevelManager levelManager)
        {
            this.levelManager = levelManager;

            red = Instantiate(RedPrefab, levelManager.PipePositionAt(RedStart.x, RedStart.y), Quaternion.identity, levelManager.Parent).transform;
            blue = Instantiate(BluePrefab, levelManager.PipePositionAt(BlueStart.x, BlueStart.y), Quaternion.identity, levelManager.Parent).transform;

            monkey1 = Instantiate(Monkey1Prefab, levelManager.PipePositionAt(RedStart.x, 0)+new Vector3(0,levelManager.spacing*1.5f), Quaternion.identity, levelManager.Parent).transform;
            monkey2 = Instantiate(Monkey2Prefab, levelManager.PipePositionAt(BlueStart.x, levelManager.lastIndY) + new Vector3(0, -levelManager.spacing * 1.5f), Quaternion.identity, levelManager.Parent).transform;

            buttonMoveDelay.Add(red, 0.0f);
            buttonMoveDelay.Add(blue, 0.0f);
            buttonRotateDelay.Add(red, 0.0f);
            buttonRotateDelay.Add(blue, 0.0f);
            buttonAccelState.Add(red, 0);
            buttonAccelState.Add(blue, 0);
        }

        private void Update()
        {
            if (levelManager == null) {
              return;
            }
            buttonMoveDelay = buttonMoveDelay.Select(kvp => new KeyValuePair<Transform, float>(kvp.Key, kvp.Value + Time.deltaTime)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            buttonRotateDelay = buttonRotateDelay.Select(kvp => new KeyValuePair<Transform, float>(kvp.Key, kvp.Value + Time.deltaTime)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            buttonShipDelay[0] = buttonShipDelay[0] + Time.deltaTime;
            buttonShipDelay[1] = buttonShipDelay[1] + Time.deltaTime;

            if(!inputEnabled) return;

            var playerAState = GamePad.GetState(PlayerIndex.One);
            var playerBState = GamePad.GetState(PlayerIndex.Two);

            var playerAMoveX = playerAState.Left.x;
            var playerAMoveY = playerAState.Left.y;
            var playerBMoveX = playerBState.Left.x;
            var playerBMoveY = playerBState.Left.y;

            var rotateALeft = playerAState.Pressed(CButton.A);
            var rotateARight = playerAState.Pressed(CButton.B);

            var rotateBLeft = playerBState.Pressed(CButton.A);
            var rotateBRight = playerBState.Pressed(CButton.B);

            var playerAMoveShip = -playerAState.Right.y;
            var playerBMoveShip = -playerBState.Right.y;

            if (Math.Abs(playerAMoveX) > ButtonForceNeeded || Math.Abs(playerAMoveY) > ButtonForceNeeded)
            {
                MovePlayer(red, playerAMoveX, playerAMoveY, monkey1);
            } else {
                buttonAccelState[red] = 0;
            }
            if (Math.Abs(playerBMoveX) > ButtonForceNeeded || Math.Abs(playerBMoveY) > ButtonForceNeeded)
            {
                MovePlayer(blue, playerBMoveX, playerBMoveY, monkey2);
            } else {
                buttonAccelState[blue] = 0;
            }

            if (rotateALeft || rotateARight)
            {
                RotatePipe(red, rotateALeft);
            }
            if (rotateBLeft || rotateBRight)
            {
                RotatePipe(blue, rotateBLeft);
            }

            if(Math.Abs(playerAMoveShip) > 0.1f && buttonShipDelay[0] > ButtonShipDelayTime) {
                levelManager.MoveEndPoint(true, playerAMoveShip > 0);
                buttonShipDelay[0] = 0;
            }
            if(Math.Abs(playerBMoveShip) > 0.1f && buttonShipDelay[1] > ButtonShipDelayTime) {
                levelManager.MoveEndPoint(false, playerBMoveShip > 0);
                buttonShipDelay[1] = 0;
            }
        }

        private void RotatePipe(Transform player, bool left)
        {
            if (buttonRotateDelay[player] > ButtonRotateDelayTime)
            {
                var x = (int) player.position.x + levelManager.levelWidth / 2;
                var y = (int) player.position.y + levelManager.levelHeight / 2;
                levelManager.RotatePipeAt(left, x, levelManager.lastIndY - y);
                buttonRotateDelay[player] = 0;

                // sfx
                var turnSfxEmitter = player.GetComponents<FMODUnity.StudioEventEmitter>()[0];
                turnSfxEmitter.Play();
            }
        }

        private void MovePlayer(Transform player, float playerMoveX, float playerMoveY, Transform monkey)
        {
            float delayTime = ButtonMoveDelayTime;
            // BUG: if this is set to > 0, then accel will happen sooner, but it will also sometimes catch
            // after two separate moves :/
            if (player != null &&  buttonAccelState[player] > 1) {
              delayTime = ButtonMoveAccelTime;
            }
            if (player != null && buttonMoveDelay[player] > delayTime)
            {
                var x = (int) player.position.x;
                var y = (int) player.position.y;

                //var ox = (int) enemy.position.x;
                //var oy = (int) enemy.position.y;


                if (playerMoveX < -ButtonForceNeeded && x + levelManager.levelWidth / 2 > 0)
                {
                    player.position = new Vector3(x - levelManager.spacing, y, player.position.z);
                    PostMove(player, delayTime);
                    monkey.position = new Vector3(player.position.x, monkey.position.y, monkey.position.z);
                    return;
                }
                if (playerMoveX > ButtonForceNeeded && x + levelManager.levelWidth / 2 < levelManager.lastIndX)
                {
                    player.position = new Vector3(x + levelManager.spacing, y, player.position.z);
                    PostMove(player, delayTime);
                    monkey.position = new Vector3(player.position.x, monkey.position.y, monkey.position.z);
                    return;
                }
                if (playerMoveY > ButtonForceNeeded && y + levelManager.levelHeight / 2 > 0)
                {
                    player.position = new Vector3(x, y - levelManager.spacing, player.position.z);
                    PostMove(player, delayTime);
                    return;
                }
                if (playerMoveY < -ButtonForceNeeded && y + levelManager.levelHeight / 2 < levelManager.lastIndY)
                {
                    player.position = new Vector3(x, y + levelManager.spacing, player.position.z);
                    PostMove(player, delayTime);
                    return;
                }
            }
        }

        private void PostMove(Transform player, float delayTime)
        {
          if (buttonMoveDelay[player] < delayTime + ButtonMoveAccelThresholdTime) {
            buttonAccelState[player]++;
          }
          buttonMoveDelay[player] = 0;

          var moveSfxEmitter = player.GetComponents<FMODUnity.StudioEventEmitter>()[1];
          moveSfxEmitter.Play();
        }

        public void Clear()
        {

        }
    }
}
