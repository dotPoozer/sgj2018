﻿using System;
using UnityEngine;

namespace DTO
{
    [Serializable]
    public class AudioRef
    {
        public string Name;
        public AudioClip Sound;
    }
}