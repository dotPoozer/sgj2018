﻿using Services;
using UnityEngine;

public class ServiceLocator : MonoBehaviour
{
    public static ServiceLocator Instance;

    public SceneService SceneService;
    public ScoringService ScoringService;
    public WindowsService WindowsService;
    public AudioService AudioService;
    public PlayerControllerService PlayerControllerService;

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
        }

        FindIfNull(ref SceneService);
        FindIfNull(ref ScoringService);
        FindIfNull(ref WindowsService);
        FindIfNull(ref AudioService);
        FindIfNull(ref PlayerControllerService);

        SceneService.Init();
        WindowsService.Init();
    }

    private static void FindIfNull<TService>(ref TService service) where TService : MonoBehaviour
    {
        if (service == null)
        {
            Debug.LogFormat("Service {0} is null!", typeof(TService));
            service = FindObjectOfType<TService>();
        }
    }

}